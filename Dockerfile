FROM haskell:8.10-buster

RUN cabal update && cabal install happy alex

COPY ./bnfc/source /bnfc
RUN cd /bnfc && cabal install

COPY run.sh /usr/local/bin/

RUN mkdir /data
WORKDIR /data

ENTRYPOINT ["run.sh"]
