# BNFC Haskell

## Prereqs

Any modern Linux or Mac running Docker 2018 CE or greater will suffice.

## Usage

The image will attempt to generate the Haskell parser for the `grammar.cf` file:

```bash
$ docker run -v ${PWD}:/data registry.gitlab.com/yorkcs/batman/bnfc-haskell grammar
```

## Building

In standard Docker fashion:

```bash
$ docker build -t registry.gitlab.com/yorkcs/batman/bnfc-haskell:latest .
```

And then to publish:

```bash
$ docker push registry.gitlab.com/yorkcs/batman/bnfc-haskell
```
