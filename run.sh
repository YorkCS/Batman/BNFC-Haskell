#!/usr/bin/env bash

echo "Running BNFC"
bnfc --haskell --functor --ghc $1.cf
rm Doc${1^}.txt
echo "Running Happy"
happy -gca Par${1^}.y
rm Par${1^}.y
echo "Running Alex"
alex -g Lex${1^}.x
rm Lex${1^}.x
